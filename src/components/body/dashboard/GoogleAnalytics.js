import React, { useState, useEffect } from 'react';
import Chart from '../../core/Chart';
import DashboardBox from '../../core/DashboardBox';
import styles from '../../../css/DashboardBox.module.scss';

export default function GoogleAnalytics(props) {
	const [loggedIn, setLoggedIn] = useState(false);
	const [data, setData] = useState([]);
	const [chartData, setChartData] = useState([]);

	useEffect(() => {
		let chartData = [];
		if (data.length === 7) {
			sortData(data);
			data.forEach(item => {
				switch (item.day) {
					case '6daysAgo':
						chartData.push({
							name: '6 Days Ago',
							pageViews: parseInt(item.pageViews)
						});
						break;
					case '5daysAgo':
						chartData.push({
							name: '5 Days Ago',
							pageViews: parseInt(item.pageViews)
						});
						break;
					case '4daysAgo':
						chartData.push({
							name: '4 Days Ago',
							pageViews: parseInt(item.pageViews)
						});
						break;
					case '3daysAgo':
						chartData.push({
							name: '3 Days Ago',
							pageViews: parseInt(item.pageViews)
						});
						break;
					case '2daysAgo':
						chartData.push({
							name: '2 Days Ago',
							pageViews: parseInt(item.pageViews)
						});
						break;
					case '1daysAgo':
						chartData.push({
							name: '1 Day Ago',
							pageViews: parseInt(item.pageViews)
						});
						break;
					case '0daysAgo':
						chartData.push({
							name: 'Today',
							pageViews: parseInt(item.pageViews)
						});
						break;
					default:
						break;
				}
			});
			setChartData(chartData);
		}
	}, [data]);

	function sortData(currentData) {
		currentData.sort((a, b) => {
			if (a.day === 'today') {
				return -1;
			}
			if (a.day < b.day) {
				return 1;
			}
			if (a.day > b.day) {
				return -1;
			}
			return 0;
		});
		setData(currentData);
	}

	// Included in public/index.html. The || is for the test file.
	const gapi = window.gapi || { signin2: {render: () => {}} };

	useEffect(() => {
		gapi.signin2.render('googleSignInButton', {
			'scope': 'profile email',
			'width': 240,
			'height': 50,
			'longtitle': true,
			'theme': 'dark',
			'onsuccess': queryReports,
			'onfailure': handleError
		});
		// eslint-disable-next-line
	}, []);

	const viewID = '45548323';

	function queryReports() {
		setLoggedIn(true);
		for (let i = 6; i >= 0; i--) {
			gapi.client.request({
				path: '/v4/reports:batchGet',
				root: 'https://analyticsreporting.googleapis.com/',
				method: 'POST',
				body: {
					reportRequests: [
						{
							viewId: viewID,
							metrics: [
								{
									expression: 'ga:pageviews'
								}
							],
							dateRanges: [
								{
									startDate: i + 'daysAgo',
									endDate: ((i === 0) ? 'today' : (i - 1) + 'daysAgo')
								}
							]
						}
					]
				}
			})
			.then((response) => {
				setData(prevData => {
					let newData = [];
					prevData.forEach(item => {
						newData.push(item);
					});
					newData.push({
						day: i + 'daysAgo',
						pageViews: response.result.reports[0].data.totals[0].values[0]
					});
					return newData;
				});
			})
			.catch((error) => {
				alert(error)
			});
		}
	}

	function handleError() {
		console.log('There was an error while fetching your GA data.');
	}

	return (
		<DashboardBox
			title="Google Analytics - Pageviews"
		>
			{chartData.length === 7 && 
			<Chart 
				width={props.containerWidth}
				data={chartData}
				dataKey="pageViews"
			/>
			}
			{loggedIn === false &&
				(
					<>
						<p id="googleSignInButton" className={styles.googleSignInButton} />
					</>
				)
			}
		</DashboardBox>
	);
}
