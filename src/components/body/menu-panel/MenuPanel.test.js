import React from 'react';
import { render, fireEvent, waitForElement } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import 'mutationobserver-shim';

import MenuPanel from "./index";

it('Verifies that the menu panel contains the proper sections and the dropdowns open and close appropriately', async () => {
    function handleCollapse() { }

    const {getByTestId, queryByTestId} = render(
        <MenuPanel
            handleCollapse={handleCollapse}
        />
    );
    //before clicking anything make sure none of the dropdowns are showing
    expect(queryByTestId('mpd_pages')).toBeNull();
    expect(queryByTestId('mpd_leads')).toBeNull();
    expect(queryByTestId('mpd_menu')).toBeNull();
    expect(queryByTestId('mpd_gallery')).toBeNull();
    expect(queryByTestId('mpd_forms')).toBeNull();
    expect(queryByTestId('mpd_marketing')).toBeNull();
    expect(queryByTestId('mpd_assets')).toBeNull();

    //click the Pages dropdown
    fireEvent.click(getByTestId('mp_pages'));
    const pagesDropdown = await waitForElement(() => getByTestId('mpd_pages'));
    expect(pagesDropdown).toBeTruthy();

    fireEvent.click(getByTestId('mp_leads'));
    const leadsDropdown = await waitForElement(() => getByTestId('mpd_leads'));
    expect(leadsDropdown).toBeTruthy();
    expect(queryByTestId('mpd_pages')).toBeNull();

    fireEvent.click(getByTestId('mp_menu'));
    const menuDropdown = await waitForElement(() => getByTestId('mpd_menu'));
    expect(menuDropdown).toBeTruthy();
    expect(queryByTestId('mpd_pages')).toBeNull();
    expect(queryByTestId('mpd_leads')).toBeNull();

    fireEvent.click(getByTestId('mp_gallery'));
    const galleryDropdown = await waitForElement(() => getByTestId('mpd_gallery'));
    expect(galleryDropdown).toBeTruthy();
    expect(queryByTestId('mpd_menu')).toBeNull();
    expect(queryByTestId('mpd_pages')).toBeNull();
    expect(queryByTestId('mpd_leads')).toBeNull();

    fireEvent.click(getByTestId('mp_forms'));
    const formsDropdown = await waitForElement(() => getByTestId('mpd_forms'));
    expect(formsDropdown).toBeTruthy();
    expect(queryByTestId('mpd_menu')).toBeNull();
    expect(queryByTestId('mpd_pages')).toBeNull();
    expect(queryByTestId('mpd_leads')).toBeNull();
    expect(queryByTestId('mpd_gallery')).toBeNull();

    fireEvent.click(getByTestId('mp_marketing'));
    const marketingDropdown = await waitForElement(() => getByTestId('mpd_marketing'));
    expect(marketingDropdown).toBeTruthy();
    expect(queryByTestId('mpd_menu')).toBeNull();
    expect(queryByTestId('mpd_pages')).toBeNull();
    expect(queryByTestId('mpd_leads')).toBeNull();
    expect(queryByTestId('mpd_gallery')).toBeNull();
    expect(queryByTestId('mpd_forms')).toBeNull();

    fireEvent.click(getByTestId('mp_assets'));
    const assetsDropdown = await waitForElement(() => getByTestId('mpd_assets'));
    expect(assetsDropdown).toBeTruthy();
    expect(queryByTestId('mpd_menu')).toBeNull();
    expect(queryByTestId('mpd_pages')).toBeNull();
    expect(queryByTestId('mpd_leads')).toBeNull();
    expect(queryByTestId('mpd_gallery')).toBeNull();
    expect(queryByTestId('mpd_forms')).toBeNull();
    expect(queryByTestId('mpd_marketing')).toBeNull();
});