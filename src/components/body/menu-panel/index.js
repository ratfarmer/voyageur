import React, { useState, useEffect } from 'react';
import Icon from '../../core/Icon';
import MenuPanelTopLevelItem from './MenuPanelTopLevelItem';
import MenuPanelSubList from './MenuPanelSubList';
import styles from '../../../css/MenuPanel.module.scss';
import {pagesList, leadsList, menuList, galleryList, formsList, marketingList, assetsList, messagesList, reportingList} from './data';
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';
import useWindowSize from '../../../hooks/useWindowSize';

function MenuPanel(props) {
	const [width] = useWindowSize();

	const [activeElement, setActiveElement] = useState({
		topLevel: "",
		subLevel: ""
	});

	const [menuPanelIsOpen, setMenuPanelIsOpen] = useState(true);

	useEffect(() => {
		props.handleCollapse(menuPanelIsOpen);
	}, [menuPanelIsOpen, props]);

	function toggleMenuPanel(menu) {
		if(!menuPanelIsOpen){
			setMenuPanelIsOpen(true);
			document.getElementById('menuPanel').classList.remove('collapsed');
			document.getElementById('menu_panel_collapse').classList.remove('collapsed');
		}
		switch (menu) {
			case 'pages':
				if (activeElement.topLevel === 'pages') {
					setActiveElement({
						topLevel: "",
						subLevel: ""
					});
				} else {
					setActiveElement({
						topLevel: "pages",
						subLevel: ""
					});
				}
				break;
			case 'leads':
				if (activeElement.topLevel === 'leads') {
					setActiveElement({
						topLevel: "",
						subLevel: ""
					});
				} else {
					setActiveElement({
						topLevel: "leads",
						subLevel: ""
					});
				}
				break;
			case 'menu':
				if (activeElement.topLevel === 'menu') {
					setActiveElement({
						topLevel: "",
						subLevel: ""
					});
				} else {
					setActiveElement({
						topLevel: "menu",
						subLevel: ""
					});
				}
				break;
			case 'gallery':
				if (activeElement.topLevel === 'gallery') {
					setActiveElement({
						topLevel: "",
						subLevel: ""
					});
				} else {
					setActiveElement({
						topLevel: "gallery",
						subLevel: ""
					});
				}
				break;
			case 'forms':
				if (activeElement.topLevel === 'forms') {
					setActiveElement({
						topLevel: "",
						subLevel: ""
					});
				} else {
					setActiveElement({
						topLevel: "forms",
						subLevel: ""
					});
				}
				break;
			case 'marketing':
				if (activeElement.topLevel === 'marketing') {
					setActiveElement({
						topLevel: "",
						subLevel: ""
					});
				} else {
					setActiveElement({
						topLevel: "marketing",
						subLevel: ""
					});
				}
				break;
			case 'assets':
				if (activeElement.topLevel === 'assets') {
					setActiveElement({
						topLevel: "",
						subLevel: ""
					});
				} else {
					setActiveElement({
						topLevel: "assets",
						subLevel: ""
					});
				}
				break;
			case 'messages':
				if (activeElement.topLevel === 'messages') {
					setActiveElement({
						topLevel: "",
						subLevel: ""
					});
				} else {
					setActiveElement({
						topLevel: "messages",
						subLevel: ""
					});
				}
				break;
			case 'reporting':
				if (activeElement.topLevel === 'reporting') {
					setActiveElement({
						topLevel: "",
						subLevel: ""
					});
				} else {
					setActiveElement({
						topLevel: "reporting",
						subLevel: ""
					});
				}
				break;
			default:
				setActiveElement({
					topLevel: "",
					subLevel: ""
				});
				break;
		}
	}

	const handleMenuPanelOpenClick = (panelOpen) => {
		const menuPanel = document.getElementById('menuPanel');
		const menuPanelSettings = document.getElementById('menu_panel_settings');
		const collapseButton = document.getElementById('menu_panel_collapse');
		if(panelOpen){
			setMenuPanelIsOpen(false);
			menuPanel.classList.add('collapsed');
			menuPanelSettings.classList.add('collapsed');
			collapseButton.classList.add('collapsed');
			toggleMenuPanel();
		}else{
			setMenuPanelIsOpen(true);
			menuPanel.classList.remove('collapsed');
			menuPanelSettings.classList.remove('collapsed');
			collapseButton.classList.remove('collapsed');
		}
	}

	const handleSublistClick = (clicked) => {
		let clicked_item = document.getElementById(clicked);
		let selected_sibling = document.querySelectorAll('.' + styles.sub_list_selected);
		let same_clicked = false;
		selected_sibling.forEach((sibling) => {
			if (sibling === clicked_item) {
				same_clicked = true;
			}
			sibling.classList.remove(styles.sub_list_selected);
		});
		same_clicked === false ? clicked_item.classList.add(styles.sub_list_selected) : clicked_item.classList.remove(styles.sub_list_selected);
	}

	return (
		<>
			<SimpleBar style={{height: 'calc(100% - 143px)'}}>
				<ul
					id={'menu_panel'}
					className={styles.list}
				>
					<MenuPanelTopLevelItem
						type="pages"
						text="Pages"
						icon={['fas', 'file-alt']}
						handleClick={toggleMenuPanel}
						activeElement={{
							topLevel: activeElement.topLevel
						}}
					/>
					{activeElement.topLevel === "pages" &&
						<MenuPanelSubList
							listID={'mpd_pages'}
							list={pagesList} 
							handleSublistClick={handleSublistClick}
							activeElement={{
								subLevel: activeElement.subLevel
							}}
						/>
					}
					<MenuPanelTopLevelItem
						type="leads"
						text="Leads"
						icon={['fas', 'hand-holding-usd']}
						handleClick={toggleMenuPanel}
						activeElement={{
							topLevel: activeElement.topLevel
						}}
					/>
					{activeElement.topLevel === "leads" &&
						<MenuPanelSubList
							listID={'mpd_leads'}
							list={leadsList} 
							handleSublistClick={handleSublistClick}	
						/>
					}
					<MenuPanelTopLevelItem
						type="menu"
						text="Menus"
						icon={['fab', 'elementor']}
						handleClick={toggleMenuPanel}
						activeElement={{
							topLevel: activeElement.topLevel
						}}
					/>
					{activeElement.topLevel === "menu" &&
						<MenuPanelSubList
							listID={'mpd_menu'}
							list={menuList}
							handleSublistClick={handleSublistClick}
						/>
					}
					<MenuPanelTopLevelItem
						type="gallery"
						text="Gallery"
						icon={['fas', 'images']}
						handleClick={toggleMenuPanel}
						activeElement={{
							topLevel: activeElement.topLevel
						}}
					/>
					{activeElement.topLevel === "gallery" &&
						<MenuPanelSubList
							listID={'mpd_gallery'}
							list={galleryList} 
							handleSublistClick={handleSublistClick}
						/>
					}
					<MenuPanelTopLevelItem
						type="forms"
						text="Forms"
						icon={['fas', 'columns']}
						handleClick={toggleMenuPanel}
						activeElement={{
							topLevel: activeElement.topLevel
						}}
					/>
					{activeElement.topLevel === "forms" &&
						<MenuPanelSubList
							listID={'mpd_forms'}
							list={formsList} 
							handleSublistClick={handleSublistClick}
						/>
					}
					<MenuPanelTopLevelItem
						type="marketing"
						text="Marketing"
						icon={['fas', 'chart-line']}
						handleClick={toggleMenuPanel}
						activeElement={{
							topLevel: activeElement.topLevel
						}}
					/>
					{activeElement.topLevel === "marketing" &&
						<MenuPanelSubList
							listID={'mpd_marketing'}
							list={marketingList} 
							handleSublistClick={handleSublistClick}
						/>
					}
					<MenuPanelTopLevelItem
						type="assets"
						text="Assets"
						icon={['fas', 'cubes']}
						handleClick={toggleMenuPanel}
						activeElement={{
							topLevel: activeElement.topLevel
						}}
					/>
					{activeElement.topLevel === "assets" &&
						<MenuPanelSubList
							listID={'mpd_assets'}
							list={assetsList}
							handleSublistClick={handleSublistClick}
						/>
					}
					<MenuPanelTopLevelItem
						type="messages"
						text="Messages"
						icon={['fas', 'envelope']}
						handleClick={toggleMenuPanel}
						activeElement={{
							topLevel: activeElement.topLevel
						}}
					/>
					{activeElement.topLevel === "messages" &&
					<MenuPanelSubList
						listID={'mpd_messages'}
						list={messagesList}
						handleSublistClick={handleSublistClick}
					/>
					}
					<MenuPanelTopLevelItem
						type="reporting"
						text="Reporting"
						icon={['fas', 'chart-bar']}
						handleClick={toggleMenuPanel}
						activeElement={{
							topLevel: activeElement.topLevel
						}}
					/>
					{activeElement.topLevel === "reporting" &&
					<MenuPanelSubList
						listID={'mpd_reporting'}
						list={reportingList}
						handleSublistClick={handleSublistClick}
					/>
					}
				</ul>
			</SimpleBar>
			<div id={'menu_panel_bottom'} className={styles.panel_bottom}>
				<div id={'menu_panel_collapse'} onClick={() => handleMenuPanelOpenClick(menuPanelIsOpen)}>
					<Icon icon={['fas', 'arrow-left']} />
				</div>
				<div id={'menu_panel_settings'}>
					<Icon icon={['fas', 'cog']} /> 
					{width >= 992 &&
						<span> Settings</span>
					}
				</div>
			</div>
		</>
	);
}

export default MenuPanel;