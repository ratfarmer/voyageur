import React from 'react';
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';

export default function ListPanel() {
    return (
        <>
            <h2>List Panel</h2>
            <SimpleBar style={{height: 'calc(100% - 95px)'}}>
                <p>List Panel Item</p>
                <p>List Panel Item</p>
                <p>List Panel Item</p>
                <p>List Panel Item</p>
                <p>List Panel Item</p>
                <p>List Panel Item</p>
                <p>List Panel Item</p>
                <p>List Panel Item</p>
            </SimpleBar>
        </>
    );
}
