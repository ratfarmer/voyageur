import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import 'mutationobserver-shim';

import DashboardBox from './DashboardBox';

it('Verifies DashboardBox displays properly', async () => {
    const { getByTestId } = render(
        <DashboardBox 
            title="A Dashboard Box"
        >
            <p>Some dashboard text</p>
            <p>Some more dashboard text</p>
        </DashboardBox>
    );

    expect(getByTestId('dashboardTitle').innerHTML).toEqual('A Dashboard Box');
    expect(getByTestId('dashboardContent').children).toHaveLength(2);
});