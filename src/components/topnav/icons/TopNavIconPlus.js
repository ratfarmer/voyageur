import React from 'react';
import TopNavIcon from "./TopNavIcon";

export default function TopNavIconPlus(props) {
	return (
		<TopNavIcon topNavIcon={props.clicked ? ['far', 'plus-square'] : ['fas', 'plus-square']} class={props.class}/>
	);
}
