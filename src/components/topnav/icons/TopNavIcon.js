import React from 'react';
import Icon from '../../core/Icon';

export default function TopNavIcon(props) {
	return (
		<Icon icon={props.topNavIcon} class={props.class}/>
	);
}
