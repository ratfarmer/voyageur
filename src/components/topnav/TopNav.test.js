import React from 'react';
import { render, fireEvent, waitForElement } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import 'mutationobserver-shim';
import { BrowserRouter as Router } from 'react-router-dom';

import TopNav from './TopNav';

it('Verifies flyouts in top nav open and close properly', async () => {
    const { getByTestId, queryByTestId } = render(
        <Router>
            <TopNav
                bodyClicked={false}
            />
        </Router>
    );

    // Before clicking anything, no flyouts should be showing
    expect(queryByTestId('tnf_plus')).toBeNull();
    expect(queryByTestId('tnf_bell')).toBeNull();
    expect(queryByTestId('tnf_avatar')).toBeNull();

    // Click the plus icon
    fireEvent.click(getByTestId('tn_plus'));
    const plusNodeFlyout = await waitForElement(() => getByTestId('tnf_plus'));
    expect(plusNodeFlyout).toBeTruthy();

    // Click the message icon
    fireEvent.click(getByTestId('tn_message'));
    expect(queryByTestId('tnf_plus')).toBeNull();

    // Click the bell icon
    fireEvent.click(getByTestId('tn_bell'));
    const bellNodeFlyout = await waitForElement(() => getByTestId('tnf_bell'));
    expect(queryByTestId('tnf_plus')).toBeNull();
    expect(bellNodeFlyout).toBeTruthy();

    // Click the Avatar icon
    fireEvent.click(getByTestId('tn_avatar'));
    const avatarNodeFlyout = await waitForElement(() => getByTestId('tnf_avatar'));
    expect(queryByTestId('tnf_plus')).toBeNull();
    expect(queryByTestId('tnf_bell')).toBeNull();
    expect(avatarNodeFlyout).toBeTruthy();

    // Click the help icon
    fireEvent.click(getByTestId('tn_help'));
    expect(queryByTestId('tnf_avatar')).toBeNull();
});