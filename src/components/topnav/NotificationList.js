import React, { useState } from 'react';
import { notificationsData } from './data';
import NotificationListItem from './NotificationListItem';
import styles from '../../css/NotificationList.module.scss';

export default function NotificationList() {
    const [data, setData] = useState(notificationsData);

    let initialNewCount = 0;
    data.forEach(notification => {
        if (notification.data.new) {
            initialNewCount++;
        }
    });

    const [unreadNotificationsCount, setUnreadNotificationsCount] = useState(initialNewCount);
    
    function markRead(listID) {
        let newData = [];

        data.forEach((item) => {
            if (item.id === listID) {
                if (item.data.new) {
                    setUnreadNotificationsCount(prevUnreadNotificationsCount => prevUnreadNotificationsCount - 1);
                }
                newData.push({
                    "id": listID,
                    "data": {
                        "text": item.data.text,
                        "date": item.data.date,
                        "new": false
                    }
                });
            } else {
                newData.push(
                    item
                );
            }
        });

        setData(newData);
    }

    const listItems = data.map((item) =>
        <NotificationListItem
            key={item.id}
            id={item.id}
            handleClick={markRead}
            text={item.data.text}
            date={item.data.date}
            new={item.data.new}
        />
    );

    return (
        <>
            <ul className={styles.notificationList}>
                {listItems}
            </ul>
            <div className={styles.notificationSummary}>
                <span>View All 
                    {unreadNotificationsCount > 0 ? 
                    <span 
                        className={styles.unreadNotifications}
                    > ({unreadNotificationsCount} new)</span> 
                    : ""}</span>
            </div>
        </>
    );
}
