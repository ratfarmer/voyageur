import React from 'react';
import '../../css/Avatar.scss';
import styles from '../../css/Avatar.module.scss';
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';
import { Link } from 'react-router-dom';
import useWindowSize from '../../hooks/useWindowSize';
import SearchInput from '../core/SearchInput';

function AvatarFlyout() {
	const [, windowHeight] = useWindowSize();
	const height = windowHeight < 700 ? windowHeight - 100 : 600;
	const simpleBarHeight = height - 58;

	function AvatarSearch() {
		return (
			<>
				<div id={'avatar-search'}>
					<form id={'avatar-search-form'} className={styles.search_form}>
						<SearchInput
							inputID="avatarSearchInput"
						/>
					</form>
				</div>
			</>
		);
	}

	return (
		<>
			<div id={'avatar-flyout-wrapper'} style={{ height: height }}>
				<div
					id={'avatar-flyout-top'}>
					<AvatarSearch />
					<div className={styles.top_links}>
						<Link to="/">Profile</Link>
						<Link to="login">Log out</Link>
					</div>
				</div>
				<div id={'avatar-flyout-bottom'}>
					<SimpleBar className='resizedFlyouts' style={{ height: simpleBarHeight }}>
						<div className={styles.left}>
							<ul>
								<li>Parent Co</li>
								<li>resized height - {height}</li>
								<li>window height - {window.innerHeight}</li>
								<li>Company</li>
								<li>Company</li>
								<li>Company</li>
								<li>Company</li>
								<li>Company</li>
								<li>Company</li>
								<li>Company</li>
								<li>Company</li>
								<li>Company</li>
								<li>Company</li>
								<li>Company</li>
								<li>Company</li>
								<li>Company</li>
								<li>Company</li>
							</ul>
						</div>
					</SimpleBar>
					<SimpleBar style={{ width: 356, height: simpleBarHeight }}>
						<div className={styles.right}>
							<ul>
								<li>Site</li>
								<li>Company</li>
								<li>Company</li>
								<li>Company</li>
							</ul>
						</div>
					</SimpleBar>
				</div>
			</div>
		</>
	);
}

export default AvatarFlyout;
