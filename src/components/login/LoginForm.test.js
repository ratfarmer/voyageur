import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import 'mutationobserver-shim';
import { BrowserRouter as Router } from 'react-router-dom';

import LoginForm from './LoginForm';
import { act } from 'react-dom/test-utils';

it('Verifies flyouts in top nav open and close properly', async () => {
    const { getByTestId } = render(
        <Router>
            <LoginForm />
        </Router>
    );

    // Before clicking anything, no inputs should have an error indicator
    expect(getByTestId('login-form-email').classList.contains('input-error')).toBeFalsy();
    expect(getByTestId('login-form-password').classList.contains('input-error')).toBeFalsy();
    expect(getByTestId('login-form-submit-button').classList.contains('input-error')).toBeFalsy();

    // Simulate invalid email form submission
    await act(async () => {
        fireEvent.change(getByTestId('login-form-email'), { target: { value: 'voyageur' } });
    });
    await act(async () => {
        fireEvent.change(getByTestId('login-form-password'), { target: { value: '12345' } });
    });

    await act(async () => {
        fireEvent.submit(getByTestId('login-form'));
    });
    expect(getByTestId('login-form-email').classList.contains('input-error')).toBeTruthy();
    expect(getByTestId('login-form-password').classList.contains('input-error')).toBeFalsy();

    // Simulate invalid password form submission
    await act(async () => {
        fireEvent.change(getByTestId('login-form-email'), { target: { value: 'voyageur@ecreativeworks.com' } });
    });
    await act(async () => {
        fireEvent.change(getByTestId('login-form-password'), { target: { value: '1234' } });
    });

    await act(async () => {
        fireEvent.submit(getByTestId('login-form'));
    });
    expect(getByTestId('login-form-email').classList.contains('input-error')).toBeFalsy();
    expect(getByTestId('login-form-password').classList.contains('input-error')).toBeTruthy();
});