import React from 'react';

const user = {
    isLoggedIn: true,
    firstName: "",
    lastName: "",
    email: ""
};

export const UserContext = React.createContext(user);