import React, { useState, useEffect } from 'react';
import TopNav from '../components/topnav/TopNav';
import Body from '../components/body';

export default function App() {
	const [bodyClicked, setBodyClicked] = useState(false);

	useEffect(() => {
		if (bodyClicked === true) {
			setBodyClicked(false);
		}
	}, [bodyClicked]);

	function sendClick() {
		setBodyClicked(true);
	}
	
	return (
		<div className={'no-gutters row stretch-height'}>
			<div className={'col-12 no-gutters stretch-height'}>
				<TopNav
					bodyClicked={bodyClicked}
				/>
				<Body
					handleBodyClick={sendClick}
				/>
			</div>
		</div>
	);
}